package ru.t1.aayakovlev.tm.service.impl;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.endpoint.ConnectionProvider;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyServiceImpl implements ConnectionProvider {

    @Value("#{environment['application.port']}")
    private String port;

    @Value("#{environment['application.server']}")
    private String host;

}
