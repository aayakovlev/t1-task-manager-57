package ru.t1.aayakovlev.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface ConnectionService {

    @NotNull
    Liquibase getLiquibase();

}
