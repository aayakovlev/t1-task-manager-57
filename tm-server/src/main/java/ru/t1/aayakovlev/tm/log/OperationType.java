package ru.t1.aayakovlev.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
