package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.request.TaskShowByIdRequest;
import ru.t1.aayakovlev.tm.dto.response.TaskShowByIdResponse;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class TaskShowByIdListener extends AbstractTaskListener {

    public static final String DESCRIPTION = "Show task by id.";

    public static final String NAME = "task-show-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskShowByIdListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(id);

        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        @Nullable final TaskDTO task = response.getTask();

        showTask(task);
    }

}
