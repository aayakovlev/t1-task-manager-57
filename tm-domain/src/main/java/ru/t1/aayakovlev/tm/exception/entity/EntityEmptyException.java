package ru.t1.aayakovlev.tm.exception.entity;

public class EntityEmptyException extends AbstractEntityException {

    public EntityEmptyException() {
        super("Error! Entity is empty...");
    }

}
