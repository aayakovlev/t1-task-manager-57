package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.TaskClearRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    public static final String NAME = "task-clear";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskClearListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CLEAR TASKS]");

        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());

        taskEndpoint.clearTask(request);
    }

}
