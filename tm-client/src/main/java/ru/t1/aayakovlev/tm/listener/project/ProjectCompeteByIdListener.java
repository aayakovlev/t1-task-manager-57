package ru.t1.aayakovlev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.ProjectCompleteByIdRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class ProjectCompeteByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Complete project by id.";

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectCompeteByIdListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("Enter id: ");
        @NotNull final String id = nextLine();

        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setId(id);

        projectEndpoint.completeProjectById(request);
    }

}
