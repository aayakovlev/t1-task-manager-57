package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;

@Service
public final class SessionDTOServiceImpl extends AbstractExtendedDTOService<SessionDTO, SessionDTORepository>
        implements SessionDTOService {

    @NotNull
    @Override
    protected SessionDTORepository getRepository() {
        return context.getBean(SessionDTORepository.class);
    }

}
