//package ru.t1.aayakovlev.tm.service;
//
//import liquibase.Liquibase;
//import liquibase.exception.LiquibaseException;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.*;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
//import ru.t1.aayakovlev.tm.marker.UnitCategory;
//import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
//import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
//import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
//import ru.t1.aayakovlev.tm.service.dto.impl.ProjectDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
//import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
//import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;
//
//@Category(UnitCategory.class)
//public final class ProjectServiceImplTest extends AbstractSchemeTest {
//
//
//    @NotNull
//    private static PropertyService propertyService;
//
//    @NotNull
//    private static ConnectionService connectionService;
//
//    @NotNull
//    private static ProjectDTOService service;
//
//    @NotNull
//    private static UserDTOService userService;
//
//    @BeforeClass
//    public static void initConnectionService() throws LiquibaseException {
//        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        propertyService = new PropertyServiceImpl();
//        connectionService = new ConnectionServiceImpl(propertyService);
//        service = new ProjectDTOServiceImpl(connectionService);
//        userService = new UserDTOServiceImpl(connectionService, propertyService);
//    }
//
//    @AfterClass
//    public static void destroyConnection() {
//        connectionService.close();
//    }
//
//    @Before
//    public void initData() throws AbstractException {
//        userService.save(COMMON_USER_ONE);
//        userService.save(ADMIN_USER_ONE);
//        service.save(PROJECT_USER_ONE);
//        service.save(PROJECT_USER_TWO);
//    }
//
//    @After
//    public void afterClear() throws AbstractException {
//        service.clear();
//        userService.clear();
//    }
//
//    @Test
//    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
//        @Nullable final ProjectDTO project = service.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
//        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
//        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
//        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
//        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
//    }
//
//    @Test
//    public void When_FindByIdExistsProject_Expect_ThrowsEntityNotFoundException() {
//        Assert.assertThrows(EntityNotFoundException.class,
//                () -> service.findById(USER_ID_NOT_EXISTED, PROJECT_ID_NOT_EXISTED)
//        );
//    }
//
//    @Test
//    public void When_SaveNotNullProject_Expect_ReturnProject() throws AbstractException {
//        @NotNull final ProjectDTO savedProject = service.save(PROJECT_ADMIN_ONE);
//        Assert.assertNotNull(savedProject);
//        Assert.assertEquals(PROJECT_ADMIN_ONE, savedProject);
//        @Nullable final ProjectDTO project = service.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getName(), project.getName());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getStatus(), project.getStatus());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getDescription(), project.getDescription());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getCreated(), project.getCreated());
//        Assert.assertEquals(PROJECT_ADMIN_ONE.getId(), project.getId());
//    }
//
//    @Test
//    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
//        final int count = service.count(COMMON_USER_ONE.getId());
//        Assert.assertEquals(2, count);
//    }
//
//    @Test
//    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
//        Assert.assertNotNull(service.save(PROJECT_ADMIN_TWO));
//        service.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
//    }
//
//    @Test
//    public void When_RemoveAll_Expect_ZeroCountProjects() throws AbstractException {
//        service.save(PROJECT_ADMIN_ONE);
//        service.save(PROJECT_ADMIN_TWO);
//        service.clear(ADMIN_USER_ONE.getId());
//        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
//    }
//
//    @Test
//    public void When_RemoveByIdExistedProject_Expect_Project() throws AbstractException {
//        Assert.assertNotNull(service.save(PROJECT_ADMIN_TWO));
//        service.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
//    }
//
//    @Test
//    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() throws AbstractException {
//        Assert.assertThrows(EntityNotFoundException.class,
//                () -> service.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId())
//        );
//    }
//
//    @Test
//    public void When_CreateNameProject_Expect_ExistedProject() throws AbstractException {
//        @Nullable final ProjectDTO project = service.create(ADMIN_USER_ONE.getId(), NAME);
//        Assert.assertNotNull(project);
//        Assert.assertEquals(NAME, project.getName());
//    }
//
//    @Test
//    public void When_CreateNameDescriptionProject_Expect_ExistedProject() throws AbstractException {
//        @Nullable final ProjectDTO project = service.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
//        Assert.assertNotNull(project);
//        Assert.assertEquals(NAME, project.getName());
//        Assert.assertEquals(DESCRIPTION, project.getDescription());
//    }
//
//    @Test
//    public void When_UpdateByIdProject_Expect_UpdatedProject() throws AbstractException {
//        service.save(PROJECT_ADMIN_TWO);
//        @Nullable ProjectDTO project = service.update(
//                PROJECT_ADMIN_TWO.getUserId(), PROJECT_ADMIN_TWO.getId(), NAME, DESCRIPTION
//        );
//        Assert.assertEquals(NAME, project.getName());
//        Assert.assertEquals(DESCRIPTION, project.getDescription());
//    }
//
//}
